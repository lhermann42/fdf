/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lhermann <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/18 11:46:53 by lhermann          #+#    #+#             */
/*   Updated: 2017/05/11 16:24:06 by lhermann         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FDF_H
# define FDF_H

#include "mlx.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "../libft/libft.h"
#include "../libft/get_next_line.h"

typedef struct s_pos
{
	double x;
	double y;
}			t_pos;

typedef struct s_mlx
{
	void *mlx;
	void *win;
	void *img;
	double **map;
	char *data;
	int bpp;
	int line;
	int endiant;
	int nbcol;
	int  nblin;
	int **tableau;
	t_pos	pos;
}			t_mlx;

void	ho_z_to_z(t_mlx *mlx, int x);
void	ho_z_to_o(t_mlx *mlx, int x);
void	ho_o_to_z(t_mlx *mlx, int x);
void	ve_z_to_z(t_mlx *mlx, int x);
void	ve_z_to_o(t_mlx *mlx, int x);
void	ve_o_to_z(t_mlx *mlx, int x);
char 	*readsujet(char *str);
void	counting(t_mlx *mlx,char *str);
int        **createtab(int nblin, int nbcol);
void	**refilltab(t_mlx *mlx, char *str, int **tableau);
int		ft_atoifdf(const char *str, int i);
void	horizontale(t_mlx *mlx);
void	verticale(t_mlx *mlx);
void	navigation_x(t_mlx *mlx, int **tableau);
void	navigation_y(t_mlx *mlx, int **tableau);

#endif
