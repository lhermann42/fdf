/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoifdf.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lhermann <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/10 23:29:47 by lhermann          #+#    #+#             */
/*   Updated: 2016/11/16 05:06:03 by lhermann         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

int		ft_atoifdf(const char *str, int i)
{
	int res;
	int n;

	n = 1;
	i = 0;
	res = 0;
	while ((*str[i] == ' ' || *str[i] == '\t' || *str[i] == '\n' || *str[i] == '\v' ||
				*str[i] == '\r' || *str[i] == '\f'))
		str[i]++;
	if (*str[i] == '-')
	{
		n = -n;
		str[i]++;
	}
	else if (*str[i] == '+')
		str[i]++;
	while (*str[i] >= '0' && *str[i] <= '9')
	{
		res *= 10;
		res += *str[i] - '0';
		str[i]++;
	}
	return (res * n);
}
