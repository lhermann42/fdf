/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tracing2.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lhermann <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/24 14:41:52 by lhermann          #+#    #+#             */
/*   Updated: 2017/05/24 14:41:54 by lhermann         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	ve_o_to_z(t_mlx *mlx, int x)
{
	int i;

	i = 0;
	while (i < 37.5)
	{
		mlx_pixel_put(mlx->mlx, mlx->win, mlx->pos.x, mlx->pos.y, 0xFFFFFFF);
		mlx->pos.x += 0.4;
		mlx->pos.y += .55;
		i++;
	}
}
