/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   triage.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lhermann <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/24 14:25:42 by lhermann          #+#    #+#             */
/*   Updated: 2017/05/24 14:25:44 by lhermann         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	navigation_x(t_mlx *mlx, int **tableau)
{
	int x;
	int y;

	x = 0;
	y = 0;
	while (tableau[y][x] != '\0')
	{
		if (tableau[y][x] == '\n')
		{
			y++;
			x = 0;
		}
		if (tableau[y][x] == tableau[y][x + 1])
		{
			printf("test7\n");
			ho_z_to_z(mlx, 1);
			printf("test8\n");
			x++;
			printf("test9\n");
		}
		if (tableau[y][x] < tableau[y][x + 1])
		{
			printf("test\n");
			ho_z_to_o(mlx, 1);
			x++;
			printf("test2\n");
		}
		if (tableau[y][x] > tableau[y][x + 1])
		{
			printf("test3\n");
			ho_o_to_z(mlx, 1);
			x++;
			printf("test4\n");
		}
		printf("test5\n");
	}
	printf("test6\n");
}

void	navigation_y(t_mlx *mlx, int **tableau)
{
	int x;
	int y;

	x = 0;
	y = 0;
	while (tableau[y][x] != '\0')
	{
		if (tableau[y][x] == '\n')
		{	
			x++;
			y = 0;
		}
		if (tableau[y][x] == tableau[y + 1][x])
		{
			ve_z_to_z(mlx, 1);
			y++;
		}
		if (tableau[y][x] < tableau[y + 1][x])
		{
			ve_z_to_o(mlx, 1);
			y++;
		}
		if (tableau[y][x] > tableau[y + 1][x])
		{
			ve_o_to_z(mlx, 1);
			y++;
		}
	}
}
