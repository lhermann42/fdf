/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tracing.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lhermann <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/24 14:40:17 by lhermann          #+#    #+#             */
/*   Updated: 2017/05/24 14:40:19 by lhermann         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


#include "fdf.h"

void	ho_z_to_z(t_mlx *mlx, int x)
{
	int i;

	i = 0;
	while (i < 30)
	{
		mlx_pixel_put(mlx->mlx, mlx->win, mlx->pos.x, mlx->pos.y, 0xFFFFFFF);
		mlx->pos.x += 1;
		mlx->pos.y += .3;
		i++;
	}
}

void	ho_z_to_o(t_mlx *mlx, int x)
{
	int i;

	i = 0;
	while (i < (x *37.5))
	{
		mlx_pixel_put(mlx->mlx, mlx->win, mlx->pos.x, mlx->pos.y, 0xFFFFFFF);
		mlx->pos.x += (0.4 / x);
		mlx->pos.y += 1;
		i++;
	}
}

void	ho_o_to_z(t_mlx *mlx, int x)
{
	int i;

	i = 0;
	while (i < 30)
	{
		mlx_pixel_put(mlx->mlx, mlx->win, mlx->pos.x, mlx->pos.y, 0xFFFFFFF);
		mlx->pos.x += 0.5;
		mlx->pos.y -= 1;
		i++;
	}
}

void	ve_z_to_z(t_mlx *mlx, int x)
{
	int i;

	i = 0;
	while (i < 30)
	{
		mlx_pixel_put(mlx->mlx, mlx->win, mlx->pos.x, mlx->pos.y, 0xFFFFFFF);
		mlx->pos.x += 1;
		mlx->pos.y -= .65;
		i++;
	}
}

void	ve_z_to_o(t_mlx *mlx, int x)
{
	int i;

	i = 0;
	while (i < 37.5)
	{
		mlx_pixel_put(mlx->mlx, mlx->win, mlx->pos.x, mlx->pos.y, 0xFFFFFFF);
		mlx->pos.x += 0.4;
		mlx->pos.y -= 1;
		i++;
	}
}
