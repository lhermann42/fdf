/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lhermann <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/18 18:15:56 by lhermann          #+#    #+#             */
/*   Updated: 2017/05/11 16:24:23 by lhermann         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

char	*readsujet(char *str)
{
	int			fd;
	int			len;
	char		*new;

	if (!(new = (char *)malloc(sizeof(char) * 5000000)))
		return (NULL);
	if (!(fd = open(str, O_RDONLY)))
		return (NULL);
	if (fd == -1)
		return (NULL);
	len = read(fd, new, 5000000);
	if (len == -1)
		return (NULL);
	new[++len] = '\0';
	if (close(fd) == -1)
		return (NULL);
	return (new);
}

void	counting(t_mlx *mlx, char *str)
{
	int i;
	int lin;
	int col;

	i = 0;
	col = 0;
	lin = 0;
	while (str[i] != '\n')
	{
		if (str[i] == ' ')
			i++;
		else
		{
			i++;
			col++;	
		}
	}
	mlx->nbcol = col;
	i = 0;
	while (str[i] != '\0')
	{
		if (str[i] == '\n')
		{
			i++;
			lin++;
		}
		else 
		i++;
	}
	mlx->nblin = lin;
}

int        **createtab(int nblin, int nbcol)
{
	int            **tableau;
	int            *tableau2;
	int            i;

	tableau = (int **)malloc(sizeof(int*) * nbcol);
	tableau2 = (int *)malloc(sizeof(int) * nbcol * nblin);
	i = 0;
	while (i < nbcol)
	{
		tableau[i] = &tableau2[i * nblin];
		i++;
	}
	return (tableau);
}


int		numbpar(t_mlx *mlx, char *str, int i, int x, int y)
{
	int	m;

	m =	0;
	if (str[i] == '-')
	{
		i++;
	}
	while (str[i] >= '0' && str[i] <= '9')
	{
		m *= 10;
		m += (str[i] - 48);
		i++;
	}
	mlx->tableau[y][x] = m;
	return(i);
}

void		**refilltab(t_mlx *mlx, char *new, int **tableau)
{
	int x;
	int y;
	int i;

	x = 0;
	y = 0;
	i = 0;
	while(new[i] != '\0')
	{
		if(new[i] >= '0' && new[i] <= '9')
		{
			i = numbpar(mlx, new, i, x, y);
			x++;
		}
		else if(new[i] == '\n')
		{			
			x = 0;
			y++;
			i++;
		}
		else
			i++;
	}
}