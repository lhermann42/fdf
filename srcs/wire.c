/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   wire.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lhermann <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/18 16:40:56 by lhermann          #+#    #+#             */
/*   Updated: 2017/05/18 16:40:58 by lhermann         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	horizontale(t_mlx *mlx)
{
	int y;
	int x;

	y = 0;
	x = 0;
	while (x != mlx->nbcol)
	{
		if (y == mlx->nblin)
		{
			y = 0;
			x++;
			mlx->pos.x = 150 + (x * 30);
			mlx->pos.y = 550 - (x * 19);
		}
		else if ((y + 1 != mlx->nblin) && (mlx->tableau[y][x] == mlx->tableau[y + 1][x]))
		{
			ho_z_to_z(mlx, 1);
			y++;
		}
		else
		{
			y++;
		}
	}
}

void	verticale(t_mlx *mlx)
{
	int y;
	int x;

	y = 0;
	x = 0;
	while (y != mlx->nblin)
	{
		if (x == mlx->nbcol)
		{
			x = 0;
			y++;
			mlx->pos.x = 150 + (y * 30);
			mlx->pos.y = 550 + (y * 9.2);
		}
		else if ((x + 1 != mlx->nbcol) && (mlx->tableau[y][x] == mlx->tableau[y][x + 1]))
		{
			ve_z_to_z(mlx, 1);
			x++;
		}
		else
		{
			x++;
		}
	}
}
