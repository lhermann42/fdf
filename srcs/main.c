/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lhermann <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/14 17:37:43 by lhermann          #+#    #+#             */
/*   Updated: 2017/05/18 16:59:31 by lhermann         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

int 	get_keypress(int keycode)
{
	if(keycode == 53)
		exit(0);
	return(0);
}

int		main(int ac, char **av)
{
	t_mlx mlx;
	char *str;
	char *new;
	int **tableau;


	ac = 1;
	new = readsujet(av[1]);
	counting(&mlx, new);
	mlx.tableau = createtab(mlx.nbcol, mlx.nblin);
	refilltab(&mlx, new, tableau);
	mlx.pos.x = 150;
	mlx.pos.y = 550;
	mlx.mlx = mlx_init();
	mlx.win = mlx_new_window(mlx.mlx, 750, 750, "FDF");
	//horizontale(&mlx);
	navigation_x(&mlx, tableau);
	//mlx.pos.x = 150;
	//mlx.pos.y = 550;
	navigation_y(&mlx, tableau);
	//verticale(&mlx);
	mlx_key_hook(mlx.win, &get_keypress, &mlx);
	mlx_loop(mlx.mlx); 	
	return (0);
}
